///==========================================================================
/// \file	schema.cpp
/// \brief	Wrapper for libxml's _xmlSchema
/// \date	4/1/2005
/// \author	Kyle Swaim
///==========================================================================



#ifdef	USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include <libxml/schemasInternals.h>
#include <libxml/xmlschemas.h>
#include "libxml++/schema.h"
#endif	// USE_PRECOMPILED

namespace xmlpp
{

/// \brief	Constructor
/// \param	schema - Underlying libxml schema representation
Schema::Schema( uniStr::ustring szSchemaFile )
{
	Load( szSchemaFile );
}

/// \brief	Destructor
Schema::~Schema()
{
	Unload();
}

bool Schema::Load( uniStr::ustring szSchemaFile )
{
	m_pSchemaCtxt = xmlSchemaNewParserCtxt( szSchemaFile.c_str() );
	if (!m_pSchemaCtxt)
	{
		return false;
	}

	m_pSchema = xmlSchemaParse(m_pSchemaCtxt);
	if (!m_pSchema)
	{
		Unload();
		return false;
	}

	return true;
}

void Schema::Unload()
{
	if (m_pSchema)
	{
		xmlSchemaFree( m_pSchema );
		m_pSchema = NULL;
	}

	if (m_pSchemaCtxt)
	{
		xmlSchemaFreeParserCtxt( m_pSchemaCtxt );
		m_pSchemaCtxt = NULL;
	}
}

/// \brief	Get the schema's name
/// \return	The schema's name
uniStr::ustring Schema::GetName() const
{
	return (char*)m_pSchema->name;
}

/// \brief	Get the schema's name
/// \return	The schema's name
uniStr::ustring Schema::GetId() const
{
	return (char*)m_pSchema->id;
}

/// \brief	Access the underlying libxml implementation
/// \return	the underlying libxml implementation
_xmlSchema* Schema::cobj()
{
	return m_pSchema;
}

/// \brief	Access the underlying libxml implementation
/// \return	the underlying libxml implementation
const _xmlSchema* Schema::cobj() const
{
	return m_pSchema;
}

} // namespace xmlpp