/* noncopyable.cc
 * libxml++ and this file are
 * copyright (C) 2000 by The libxml++ Development Team, and
 * are covered by the GNU Lesser General Public License, which should be
 * included with libxml++ as the file COPYING.
 */

#ifdef USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include "libxml++/noncopyable.h"
#endif

namespace xmlpp
{

NonCopyable::NonCopyable()
{
}

NonCopyable::~NonCopyable()
{
}

} //namespace xmlpp
