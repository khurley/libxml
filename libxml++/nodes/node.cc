/* xml++.cc
 * libxml++ and this file are copyright (C) 2000 by Ari Johnson, and
 * are covered by the GNU Lesser General Public License, which should be
 * included with libxml++ as the file COPYING.
 */

#ifdef USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include "libxml++/nodes/element.h"
#include "libxml++/nodes/node.h"
#include "libxml++/exceptions/internal_error.h"
#include <libxml/xpath.h>
//added "internals" to support default namespace workaround hack
#include <libxml/xpathInternals.h>
#include <libxml/tree.h>
#include <iostream>
#endif

namespace xmlpp
{

Node::Node(xmlNode* node)
  : impl_(node)
{
   impl_->_private = this;
}

Node::~Node()
{}

Node::NodeList Node::get_children(const uniStr::ustring& name)
{
   xmlNode* child = impl_->children;
   if(!child)
     return NodeList();

   NodeList children;
   do
   {
      if(child->_private)
      {
        if(name.empty() || name == (const char*)child->name)
          children.push_back(reinterpret_cast<Node*>(child->_private));
      }
      else
      {
        //This should not happen:
        //This is for debugging only:
        //if(child->type == XML_ENTITY_DECL)
        //{
        //  xmlEntity* centity = (xmlEntity*)child;
        //  std::cerr << "Node::get_children(): unexpected unwrapped Entity Declaration node name =" << centity->name << std::endl;
        //}
      }
   }
   while((child = child->next));
   
   return children;
}

const Node::NodeList Node::get_children(const uniStr::ustring& name) const
{
  return const_cast<Node*>(this)->get_children(name);
}

Element* Node::get_parent_element( void )
{
	xmlNode* temp = impl_->parent;
	while( temp != NULL )
	{
		if( temp->type == XML_ELEMENT_NODE )
		{
			return static_cast<Element*>(temp->_private);
		}
		temp = temp->parent;
	}
	return NULL;
}

Element* Node::add_child(const uniStr::ustring& name,
                         const uniStr::ustring& ns_prefix)
{
   xmlNode* node = 0;
   xmlNs* ns = 0;

   if(impl_->type != XML_ELEMENT_NODE)
      throw internal_error("You can only add child nodes to element nodes");

   //Ignore the namespace if none was specified:
   if(!ns_prefix.empty())
   {
     //Use the existing namespace if one exists:
     ns = xmlSearchNs(impl_->doc, impl_, (const xmlChar*)ns_prefix.c_str());
     if (!ns)
       throw exception("The namespace prefix (" + ns_prefix + ") has not been declared.");
   }

   node = xmlAddChild(impl_, xmlNewNode(ns, (const xmlChar*)name.c_str()));

   if(node)
     return static_cast<Element*>(node->_private);
   else
     return 0;
}

void Node::remove_child(Node* node)
{
  //TODO: Allow a node to be removed without deleting it, to allow it to be moved?
  //This would require a more complex memory management API.
  xmlUnlinkNode(node->cobj());
  xmlFreeNode(node->cobj()); //The C++ instance will be deleted in a callback.
}

Node* Node::import_node(const Node* node, bool recursive)
{
  //Create the node, by copying:
  xmlNode* imported_node = xmlDocCopyNode(const_cast<xmlNode*>(node->cobj()), impl_->doc, recursive);
  if (!imported_node)
    throw exception("Unable to import node");

  //Add the node:
  xmlNode* added_node = xmlAddChild(this->cobj(),imported_node);
  if (!added_node)
  {
    xmlFreeNode(imported_node);
    throw exception("Unable to add imported node to current node");
  }

  return static_cast<Node*>(imported_node->_private);
}

uniStr::ustring Node::get_name() const
{
  return impl_->name ? (const char*)impl_->name : "";
}

void Node::set_name(const uniStr::ustring& name)
{
  xmlNodeSetName( impl_, (const xmlChar *)name.c_str() );
}

int Node::get_line() const
{
   return XML_GET_LINE(impl_);
}


xmlNode* Node::cobj()
{
  return impl_;
}

const xmlNode* Node::cobj() const
{
  return impl_;
}

uniStr::ustring Node::get_path() const
{
  xmlChar* path = xmlGetNodePath(impl_);
  uniStr::ustring retn = path ? (char*)path : "";
  xmlFree(path);
  return retn;
}

void Node::set_namespace_with_ref(const std::string& ns_prefix, const 
std::string& ns_ref) const {
   xmlNs* ns = xmlSearchNs( cobj()->doc, const_cast<xmlNodePtr>(cobj()), 
(xmlChar*)ns_prefix.c_str() );
   if(!ns) {
       nsprefix_ = ns_prefix;
       nsref_ = ns_ref;
   } else {
       nsprefix_.clear();
       nsref_.clear();
   }
}

NodeSet Node::find(const uniStr::ustring& xpath) const
{
  xmlXPathContext* ctxt = xmlXPathNewContext(impl_->doc);
  ctxt->node = impl_;

  //BJA: XML standard does not provide for XPath to 
  //search against default namespace.  This is a hack
  //to circumvent problem, along with method set_default_prefix(), 
  //by assigning a prefix to the default namespace.
  if (!nsprefix_.empty() && !nsref_.empty()) 
  {
      xmlXPathRegisterNs( ctxt,
          reinterpret_cast<const xmlChar*>(nsprefix_.c_str()),
          reinterpret_cast<const xmlChar*>(nsref_.c_str()));
  };

  xmlXPathObject* result = xmlXPathEval((const xmlChar*)xpath.c_str(), ctxt);

  if(!result)
  {
    xmlXPathFreeContext(ctxt);
    throw exception("Invalid XPath: " + xpath);
  }

  if(result->type != XPATH_NODESET)
  {
    xmlXPathFreeObject(result);
    xmlXPathFreeContext(ctxt);
    throw internal_error("Only nodeset result types are supported.");
  }

  xmlNodeSet* nodeset = result->nodesetval;
  NodeSet nodes;
  if( nodeset )
  {
    nodes.reserve( nodeset->nodeNr );
    for (int i = 0; i != nodeset->nodeNr; ++i)
      nodes.push_back(static_cast<Node*>(nodeset->nodeTab[i]->_private));
  }
  else
  {
    // return empty set
  }

  xmlXPathFreeObject(result);
  xmlXPathFreeContext(ctxt);

  return nodes;
}

uniStr::ustring Node::get_namespace_prefix() const
{
  if(impl_ && impl_->ns && impl_->ns->prefix)
    return (char*)impl_->ns->prefix;
  else
    return uniStr::ustring();
}

uniStr::ustring Node::get_namespace_uri() const
{
   if(impl_ && impl_->ns && impl_->ns->href)
    return (char*)impl_->ns->href;
  else
    return uniStr::ustring();
}

void Node::set_namespace(const uniStr::ustring& ns_prefix)
{
  //Look for the existing namespace to use:
  xmlNs* ns = xmlSearchNs( cobj()->doc, cobj(), (xmlChar*)ns_prefix.c_str() );
  if(ns)
  {
      //Use it for this element:
      xmlSetNs(cobj(), ns);
  }
  else
  {
    throw exception("The namespace (" + ns_prefix + ") has not been declared.");
  }
}


} //namespace xmlpp
