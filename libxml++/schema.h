///==========================================================================
/// \file	schema.h
/// \brief	Wrapper for libxml's _xmlSchema
/// \date	4/1/2005
/// \author	Kyle Swaim
///==========================================================================

#ifndef	SCHEMA_H
#define SCHEMA_H

#include "libxml++/attribute.h"

#ifndef DOXYGEN_SHOULD_SKIP_THIS
extern "C" {
  struct _xmlSchema;
  struct _xmlSchemaParserCtxt;
}
#endif //DOXYGEN_SHOULD_SKIP_THIS

namespace xmlpp
{

///==========================================================================
/// \class	Schema
/// \biref	Wrapper for libxml's xmlSchema
///==========================================================================
class Schema
{
public:
	/// \brief	Constructor
	/// \param	schema - Underlying libxml schema representation
	Schema( uniStr::ustring szSchemaFile );
	/// \brief	Destructor
	~Schema();

	bool Load( uniStr::ustring szSchemaFile );
	void Unload();

	/// \brief	Get the schema's name
	/// \return	The schema's name
	uniStr::ustring GetName() const;
	/// \brief	Get the schema's name
	/// \return	The schema's name
	uniStr::ustring GetId() const;

	/// \brief	Access the underlying libxml implementation
	/// \return	the underlying libxml implementation
	_xmlSchema* cobj();
	/// \brief	Access the underlying libxml implementation
	/// \return	the underlying libxml implementation
	const _xmlSchema* cobj() const;

private:
	_xmlSchemaParserCtxt *m_pSchemaCtxt;
	_xmlSchema *m_pSchema;
};

} // namespace xmlpp

#endif	// SCHEMA_H