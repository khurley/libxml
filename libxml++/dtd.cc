/* xml++.cc
 * libxml++ and this file are copyright (C) 2000 by Ari Johnson, and
 * are covered by the GNU Lesser General Public License, which should be
 * included with libxml++ as the file COPYING.
 */

#ifdef USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include "libxml++/dtd.h"
#include <libxml/tree.h>
#endif

namespace xmlpp
{
  
Dtd::Dtd(_xmlDtd* dtd)
: impl_(dtd)
{
  dtd->_private = this;
}

Dtd::~Dtd()
{ 
}

uniStr::ustring Dtd::get_name() const
{
  return (char*)impl_->name;
}

uniStr::ustring Dtd::get_external_id() const
{
  return (char*)impl_->ExternalID;
}

uniStr::ustring Dtd::get_system_id() const
{
  return (char*)impl_->SystemID;
}

_xmlDtd* Dtd::cobj()
{
  return impl_;
}

const _xmlDtd* Dtd::cobj() const
{
  return impl_;
}

} //namespace xmlpp
