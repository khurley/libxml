///==========================================================================
/// \file	schemavalidator.h
/// \brief	Validate an XML file using a schema
/// \date	4/5/2005
/// \author	Kyle Swaim
///==========================================================================

#ifndef	SCHEMAVALIDATOR_H
#define	SCHEMAVALIDATOR_H

#ifndef USE_PRECOMPILED
#include "libxml++/validators/validator.h"
#include "libxml++/schema.h"
#include "libxml++/document.h"
#endif

namespace xmlpp
{

class SchemaValidator : public Validator
{
public:
	/// \brief	Default Constructor
	SchemaValidator();
	/// \brief	Constructor
	explicit SchemaValidator( const uniStr::ustring& file );
	/// \brief	Constructor
	explicit SchemaValidator( const uniStr::ustring& external, const uniStr::ustring& system );
	/// \brief Destructor
	virtual ~SchemaValidator();

	/// \brief Set the schema
	/// \param pSchema - the schema
	void set_schema( Schema *pSchema );

	/// \brief	Get the schema
	/// \return The schema
	Schema* get_schema();
	/// \brief	Get the schema
	/// \return The schema
	const Schema* get_schema() const;

	/// \brief	Parse a subset of a file
	/// \param
	/// \param
	virtual void parse_subset( const uniStr::ustring& external, const uniStr::ustring& system );
	/// \brief	Parse a file
	/// \param	filename - the name of the file to parse
	virtual void parse_file( const uniStr::ustring& filename );
	/// \brief	Parse a section of memory
	/// param	contents - The section of memory to parse
	virtual void parse_memory( const uniStr::ustring& contents );
	/// \brief	Parse an IO stream
	/// \param	in - The input stream to parse
	virtual void parse_stream( std::istream& in );

	/// \brief	Validate a document against the schema
	/// \param	doc - The document to validate
	/// \return	true if the doc is valid, false if it is not
	bool validate( Document* doc );

	/// \brief Validate a node against the schema
	/// \param pNode - The node to validate
	/// \return true if the node is valid, false otherwise
	bool validate( Node *pNode );

	static void xmlSchemaValidityErrorFunc (void *ctx, const char *msg, ...);
	static void xmlSchemaValidityWarningFunc (void *ctx, const char *msg, ...);

protected:
	/// \brief	Release the underlying schema representation
	virtual void release_underlying();

	Schema* m_Schema;
};

} // namespace xmlpp

#endif	// SCHEMAVALIDATOR_H