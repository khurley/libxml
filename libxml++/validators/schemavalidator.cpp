///==========================================================================
/// \file	schemavalidator.cpp
/// \brief	Validate an XML file using a schema
/// \date	4/5/2005
/// \author	Kyle Swaim
///==========================================================================

#ifdef USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include "libxml++/validators/dtdvalidator.h"
#include "libxml++/dtd.h"
#include "libxml++/nodes/element.h"
#include "libxml++/nodes/textnode.h"
#include "libxml++/nodes/commentnode.h"
#include "libxml++/keepblanks.h"
#include "libxml++/exceptions/internal_error.h"
#include "libxml++/io/istreamparserinputbuffer.h"
#include <libxml/parserInternals.h>//For xmlCreateFileParserCtxt().
#include <libxml/xmlschemas.h>
#include <sstream>
#include <iostream>
#include <stdarg.h>
#endif

#include <string.h>
#include "schemavalidator.h"

namespace xmlpp
{

/// \brief	Default Constructor
SchemaValidator::SchemaValidator()
{
	m_Schema = NULL;
}

/// \brief	Constructor
SchemaValidator::SchemaValidator( const uniStr::ustring& file )
{
	m_Schema = NULL;
	parse_subset( "", file );
}

/// \brief	Constructor
SchemaValidator::SchemaValidator( const uniStr::ustring& external, const uniStr::ustring& system )
{
	m_Schema = NULL;
	parse_subset( external, system );
}

/// \brief Destructor
SchemaValidator::~SchemaValidator()
{
	release_underlying();
	Validator::release_underlying();
}

/// \brief	Parse a subset of a file
/// \param
/// \param
void SchemaValidator::parse_subset( const uniStr::ustring& external, const uniStr::ustring& system )
{
	release_underlying(); // Free any existing dtd.
//	no schema parser
//	xmlSchema* schema = xmlParseDTD( external.empty() ? 0 : (const xmlChar *)external.c_str(),
//									 system.empty() ? 0 : (const xmlChar *)system.c_str() );

	//if( ! schema )
	//	throw parse_error("Schema could not be parsed");

	//m_Schema = static_cast<Schema*>(schema->_private);
}

/// \brief	Parse a file
/// \param	filename - the name of the file to parse
void SchemaValidator::parse_file( const uniStr::ustring& filename )
{
	parse_subset( "", filename );
}
/// \brief	Parse a section of memory
/// param	contents - The section of memory to parse
void SchemaValidator::parse_memory( const uniStr::ustring& contents )
{
	// Prepare an istream with buffer
	std::istringstream is( contents );

	parse_stream( is );
}
/// \brief	Parse an IO stream
/// \param	in - The input stream to parse
void SchemaValidator::parse_stream( std::istream& in )
{
	release_underlying(); //Free any existing document.

	IStreamParserInputBuffer ibuff( in );
	// no schema parser
	//xmlSchema* schema = xmlIOParseDTD( 0, ibuff.cobj(), XML_CHAR_ENCODING_UTF8 );

	//if( !schema )
	//	throw parse_error("Schema could not be parsed");

	//m_Schema = static_cast<Schema*>(schema->_private);
}

void SchemaValidator::set_schema( Schema *pSchema )
{
	m_Schema = pSchema;
}

/// \brief	Get the schema
/// \return The schema
Schema* SchemaValidator::get_schema()
{
	return m_Schema;
}

/// \brief	Get the schema
/// \return The schema
const Schema* SchemaValidator::get_schema() const
{
	return m_Schema;
}

/// \brief	Validate a document against the schema
/// \param	doc - The document to validate
/// \return	true if the doc is valid, false if it is not
bool SchemaValidator::validate( Document* doc )
{
	xmlSchemaValidCtxtPtr pValidCtxt = xmlSchemaNewValidCtxt( m_Schema->cobj() );
	// TODO: send userdata to error/warning callbacks for proper error message output, ie. log ptr
	xmlSchemaSetValidErrors( pValidCtxt, xmlSchemaValidityErrorFunc, xmlSchemaValidityWarningFunc, NULL );

	int iRetVal = xmlSchemaValidateDoc( pValidCtxt, doc->cobj() );
// * Returns 0 if the document is schemas valid, a positive error code
// *     number otherwise and -1 in case of internal or API error.

	xmlSchemaFreeValidCtxt( pValidCtxt );

	return (iRetVal == 0);
/*
	// A context is required at this stage only
	if( !valid_ )
		valid_ = xmlNewValidCtxt();

	if( !valid_ )
	{
		throw internal_error( "Couldn't create parsing context" );
	}

	if( !doc )
		throw internal_error( "Document pointer cannot be 0" );

	initialize_valid();

	#pragma warning( disable: 4800 )
		//bool res = (bool)xmlValidateDtd( valid_, (xmlDoc*)doc->cobj(), dtd_->cobj() );
	#pragma warning( default: 4800 )

//	if(res == 0)
//	{
//		check_for_exception();
//		throw validity_error("Document failed Dtd validation");
//	}

//	return res;
	return true;
*/
}

bool SchemaValidator::validate( Node *pNode )
{
	if (!pNode)
		return false;

	xmlSchemaValidCtxtPtr pValidCtxt = xmlSchemaNewValidCtxt( m_Schema->cobj() );
	// TODO: send userdata to error/warning callbacks for proper error message output, ie. log ptr
	xmlSchemaSetValidErrors( pValidCtxt, xmlSchemaValidityErrorFunc, xmlSchemaValidityWarningFunc, NULL );

	int iRetVal = xmlSchemaValidateOneElement( pValidCtxt, pNode->cobj() );
// * Returns 0 if the document is schemas valid, a positive error code
// *     number otherwise and -1 in case of internal or API error.

	xmlSchemaFreeValidCtxt( pValidCtxt );

	return (iRetVal == 0);
}

void SchemaValidator::xmlSchemaValidityErrorFunc (void *ctx, const char *msg, ...)
{
	va_list marker;
	va_start( marker, msg );

	char szBuffer[2048];
#ifdef WIN32
	vsnprintf( szBuffer, 2048, msg, marker );
	// TODO: better output for errors and warnings
	printf( "Schema Validation Error: %s", szBuffer );
#else
	// TODO: implement vsnprintf for other platforms
	strcpy( szBuffer, "Unknown error." );
#endif

	va_end( marker );
}

void SchemaValidator::xmlSchemaValidityWarningFunc (void *ctx, const char *msg, ...)
{
	va_list marker;
	va_start( marker, msg );

	char szBuffer[2048];
#ifdef WIN32
	vsnprintf( szBuffer, 2048, msg, marker );
	printf( "Schema Validation Warning: %s", szBuffer );
#else
	// TODO: implement vsnprintf for other platforms
	strcpy( szBuffer, "Unknown warning." );
#endif

	va_end( marker );
}


/// \brief	Release the underlying schema representation
void SchemaValidator::release_underlying()
{
	if( m_Schema != NULL )
	{
		m_Schema = NULL;
	}
}

} // namespace xmlpp