/* istreamparserinputbuffer
 * this file is part of libxml++
 *
 * copyright (C) 2003 by libxml++ developer's team
 *
 * this file is covered by the GNU Lesser General Public License,
 * which should be included with libxml++ as the file COPYING.
 */

#ifdef USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include "libxml++/io/istreamparserinputbuffer.h"
#include <libxml/xmlIO.h>
#endif

namespace xmlpp
{
  IStreamParserInputBuffer::IStreamParserInputBuffer(
      std::istream & input, bool freeParserBuffer)
    : ParserInputBuffer(), input_(input), freeParserBuffer_(freeParserBuffer)
  {
  }

  IStreamParserInputBuffer::~IStreamParserInputBuffer()
  {
	  if (freeParserBuffer_)
	  {
		  xmlFreeParserInputBuffer(cobj());
	  }
  }

  int IStreamParserInputBuffer::do_read(
      char * buffer,
      int len)
  {
    int l=0;
    if(input_)
    {
      // This is the correct statement - but gcc 2.95.3 lacks this method
      //l = input_.readsome(buffer, len);
      input_.read(buffer, len);
      l = input_.gcount();
    }

    return l;
  }

  bool IStreamParserInputBuffer::do_close()
  {
    if (input_)
        return true;
      
    return false;
      
    //return input_;
  }
};
