#ifdef USE_PRECOMPILED
#include "libxml++/libxml++.h"
#else
#include "parse_error.h"
#endif
namespace xmlpp {

parse_error::parse_error(const uniStr::ustring& message)
: exception(message)
{
}

parse_error::~parse_error() throw()
{}

void parse_error::Raise() const
{
  throw *this;
}

exception* parse_error::Clone() const
{
  return new parse_error(*this);
}

} //namespace xmlpp

