#ifndef __LIBXML_WII_CONFIG__
#define __LIBXML_WII_CONFIG__

/* config.h generated manually for macos.  */

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME

/* Define if you have the ANSI C header files.  */
#define STDC_HEADERS

#define PACKAGE
#define VERSION

#undef HAVE_LIBZ
#undef HAVE_LIBM
#undef HAVE_ISINF
#undef HAVE_ISNAN
#undef HAVE_LIBHISTORY
#undef HAVE_LIBREADLINE

#define XML_SOCKLEN_T socklen_t

/* Define if you have the class function.  */
#undef HAVE_CLASS

/* Define if you have the finite function.  */
#undef HAVE_FINITE

/* Define if you have the fp_class function.  */
#undef HAVE_FP_CLASS

/* Define if you have the fpclass function.  */
#undef HAVE_FPCLASS

/* Define if you have the fprintf function.  */
#define HAVE_FPRINTF

/* Define if you have the isnand function.  */
#undef HAVE_ISNAND

/* Define if you have the localtime function.  */
#define HAVE_LOCALTIME

/* Define if you have the printf function.  */
#define HAVE_PRINTF

/* Define if you have the signal function.  */
#define HAVE_SIGNAL

/* Define if you have the snprintf function.  */
#define HAVE_SNPRINTF

/* Define if you have the sprintf function.  */
#define HAVE_SPRINTF

/* Define if you have the sscanf function.  */
#define HAVE_SSCANF

/* Define if you have the stat function.  */
#define HAVE_STAT

/* Define if you have the strdup function.  */
#define HAVE_STRDUP

/* Define if you have the strerror function.  */
#define HAVE_STRERROR

/* Define if you have the strftime function.  */
#define HAVE_STRFTIME

/* Define if you have the strndup function.  */
#define HAVE_STRNDUP

/* Define if you have the vfprintf function.  */
#define HAVE_VFPRINTF

/* Define if you have the vsnprintf function.  */
#define HAVE_VSNPRINTF

/* Define if you have the vsprintf function.  */
#define HAVE_VSPRINTF

/* Define if you have the <ctype.h> header file.  */
#define HAVE_CTYPE_H

/* Define if you have the <dirent.h> header file.  */
#define HAVE_DIRENT_H

/* Define if you have the <dlfcn.h> header file.  */
#define HAVE_DLFCN_H

/* Define if you have the <errno.h> header file.  */
#define HAVE_ERRNO_H

/* Define if you have the <fcntl.h> header file.  */
#define HAVE_FCNTL_H

/* Define if you have the <float.h> header file.  */
#define HAVE_FLOAT_H

/* Define if you have the <fp_class.h> header file.  */
#define HAVE_FP_CLASS_H

/* Define if you have the <ieeefp.h> header file.  */
#define HAVE_IEEEFP_H

/* Define if you have the <malloc.h> header file.  */
#undef HAVE_MALLOC_H

/* Define if you have the <math.h> header file.  */
#define HAVE_MATH_H

/* Define if you have the <stdlib.h> header file.  */
#define HAVE_STDLIB_H

/* Define if you have the <string.h> header file.  */
#define HAVE_STRING_H

/* Define if you have the <sys/stat.h> header file.  */
#define HAVE_SYS_STAT_H

/* Define if you have the <sys/time.h> header file.  */
#define HAVE_SYS_TIME_H

/* Define if you have the <time.h> header file.  */
#define HAVE_TIME_H

/* Name of package */
#define PACKAGE

/* Version number of package */
#define VERSION

/* Define if compiler has function prototypes */
#define PROTOTYPES

#include <libxml/xmlversion.h>
#include <types.h>

#endif /* __LIBXML_WII_CONFIG__ */

